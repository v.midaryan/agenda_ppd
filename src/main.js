import 'core-js/stable'
import Vue from 'vue'
import App from './App'
import router from './router'
import CoreuiVue from '@coreui/vue'
import {iconsSet as icons} from './assets/icons/icons.js'
import store from './store'
import axios from "axios";
import moment from 'moment'
import 'vue-search-select/dist/VueSearchSelect.css'

const base = axios.create({
  baseURL: "http://localhost:5000"
});

Vue.prototype.$http = base;
Vue.config.productionTip = false;
Vue.config.performance = true
Vue.use(CoreuiVue)
Vue.prototype.$log = console.log.bind(console)
Vue.prototype.moment = moment

new Vue({
  el: '#app',
  router,
  store,
  icons,
  template: '<App/>',
  components: {
    App
  }
})
