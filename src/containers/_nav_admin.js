export default [
    {
        _name: 'CSidebarNav',
        _children: [
            {
                _name: 'CSidebarNavItem',
                name: 'Dashboard',
                to: '/dashboard',
                icon: 'cil-speedometer'
            },
            {
                _name: 'CSidebarNavItem',
                name: 'Rechercher une salle',
                to: '/RechercheSalle',
                icon: 'cil-zoom-in'
            },
            {
                _name: 'CSidebarNavItem',
                name: 'Salle de réunion',
                to: '/CreateSalleDeReunion',
                icon: 'cil-pencil'
            },
            {
                _name: 'CSidebarNavItem',
                name: 'Ajouter un utilisateur',
                to: '/register',
                icon: 'cil-user-follow'
            },
            {
                _name: 'CSidebarNavItem',
                name: 'Utilisateurs',
                to: '/users',
                icon: 'cil-people'
            }
        ]
    }
]