export default [
    {
        _name: 'CSidebarNav',
        _children: [
            {
                _name: 'CSidebarNavItem',
                name: 'Dashboard',
                to: '/dashboard',
                icon: 'cil-speedometer'
            },
            {
                _name: 'CSidebarNavItem',
                name: 'Rechercher une salle',
                to: '/RechercheSalle',
                icon: 'cil-zoom-in'
            }
        ]
    }
]